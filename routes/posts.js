const express = require('express');
const router = express.Router();
const Post = require('../models/post');

router.get('/', async (req, res) => {

    try{
        const post = await Post.find();
        res.json(post)
    }catch(err){
        res.json({message: err})
    }

});

router.post('/', (req, res) => {
    const post = new Post({
        label: req.body.label,
        type: req.body.type,
        required: req.body.required
    });
    
    post.save()
    .then(
        data => res.json(data)
    )
    .catch(err => {
        res.json({message: err})
    })
})

module.exports = router;
