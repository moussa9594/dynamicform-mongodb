const express = require('express');
const mongoose = require('mongoose')
require('dotenv/config')
const app = express();

//import body -parser
const bodyParser = require('body-parser')
//Import Routes
const postRoute = require('./routes/posts')
const userRoute = require('./routes/users')
//Midlleware
app.use(bodyParser.json())
app.use('/post', postRoute)
app.use('/user', userRoute)

// routes
app.get('/', (req, res) => {
    res.send('we are on home');
});


//connect to db
mongoose.connect(process.env.DB_CONNECTION , { useNewUrlParser: true, useUnifiedTopology: true } , ()=> console.log('connect to DB success !!!') )

// how to use start listening to the server
app.listen(3001);