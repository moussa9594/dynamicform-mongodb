const mongoose = require('mongoose');

PostSchema = mongoose.Schema({  
    label: String, //The label of umfield
    type: String, // possible values: text(default), file to upload, checkbox, radio, select in a list
    required: Boolean //define if the field is requireed or not,
 });

module.exports = mongoose.model('postSchema', PostSchema);